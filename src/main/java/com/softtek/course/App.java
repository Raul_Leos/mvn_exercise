package com.softtek.course;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //MEtodos dummy de App
        System.out.println( "Hello World!" );
        System.out.println(getWhoRules());
        System.out.println(suma(5,6));

        //Clase calculadora agregada
        Calculadora calculadora = new Calculadora();
        calculadora.setNumero1(3.5);
        calculadora.setNumero2(4.6);
        System.out.println(calculadora.restar());
        System.out.println(calculadora.dividir());
    }

    public static String getWhoRules(){
            return "Softek Rules!!!";
    }

    public static int suma(int num1,int num2){
        return num1 + num2;
    }

}
